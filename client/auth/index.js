/**
 * Client-side authentication manager
 * Author: Don
 */

class CefHelper {
    constructor (resourcePath) {
        this.path = resourcePath
        this.open = false
    }

    show () {
        if (this.open === false) {
            this.open = true

            var resolution = API.getScreenResolution()

            this.browser = API.createCefBrowser(1920, 1080, true)
            API.waitUntilCefBrowserInit(this.browser)
            API.setCefBrowserPosition(this.browser, 0, 0)
            API.loadPageCefBrowser(this.browser, this.path)
            API.waitUntilCefBrowserLoaded(this.browser)
            API.showCursor(true)
        }
    }

    destroy () {
        this.open = false
        API.destroyCefBrowser(this.browser)
        API.showCursor(false)
    }

    eval (string) {
        this.browser.eval(string)
    }
}

const cef = new CefHelper('client/auth/login.html');

API.onServerEventTrigger.connect(function(eventName, args) {
	if(eventName == "ui_auth_prompt_login_show") {
        API.sendNotification("Beginning cef load");
		cef.show();
        var date = new Date();
		API.sendNotification("showing auth at " + date.getUTCMilliseconds());
	} else if(eventName == "ui_auth_prompt_login_destroy") {
		cef.destroy();
	}
});

function load()
{
    var date = new Date();
    API.sendNotification("Got load from browser at " + date.getUTCMilliseconds());
}

function login(username, password)
{
	API.sendNotification('Attempting logon');
	API.triggerServerEvent("auth_attempt_login", "HELLO", "WORLD");
}