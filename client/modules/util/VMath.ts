class VMath {
	static getLocalPlayerForwardPosition(range: number) {
		var position = API.getEntityPosition(API.getLocalPlayer());
		var rotation = API.getEntityRotation(API.getLocalPlayer());
		var angle = VMath.clampAngle(rotation.Z) * (Math.PI / 180);

		position.X += range * Math.sin(-angle);
		position.Z += range * Math.cos(-angle);
		return position;
	}

	static clampAngle(angle: number) {
		return (angle + Math.ceil(-angle / 360) * 360);
	}
}