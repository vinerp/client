class CefHelper {
    created: boolean;
    browser: GTANetwork.GUI.Browser;
    Name: string;

    constructor (name: string) {
        this.created = false;
        this.Name = name;
    }

    create (width = 1920, height = 1080) {
        if (!this.created) {
            this.created = true;

            var resolution = API.getScreenResolution();

            this.browser = API.createCefBrowser(resolution.Width, resolution.Height, true);
            API.waitUntilCefBrowserInit(this.browser);
            API.setCefBrowserPosition(this.browser, 0, 0);
            API.setCefBrowserHeadless(this.browser, true);
        }
    }

    setPosition(x: number, y: number) {
        if(!this.created) return;
        API.setCefBrowserPosition(this.browser, x, y);
    }

    setSize(width: number, height: number) {
        if(!this.created) return;
        API.setCefBrowserSize(this.browser, width, height);
    }

    hide() {
        if(!this.created) return;
        if(!API.getCefBrowserHeadless(this.browser)) {
            API.setCefBrowserHeadless(this.browser, true);
        }
    }

    show() {
        if(!this.created) return;
        if(API.getCefBrowserHeadless(this.browser)) {
            API.setCefBrowserHeadless(this.browser, false);
        }
    }

    goto(path: string) {
        API.loadPageCefBrowser(this.browser, path);
        //API.waitUntilCefBrowserLoaded(this.browser);
    }

    destroy () {
        this.created = false;
        API.destroyCefBrowser(this.browser);
        API.showCursor(false);
    }

    eval (string) {
        if(!this.created) return;
        this.browser.eval(string);
    }

    call(method, ...args:any[]) {
        if(!this.created) return;
        this.browser.call(method, args);
    }
}