const EVENT_MARKER_CREATE = "modules_market_create";
const EVENT_MARKER_DESTROY = "modules_marker_destroy";
const EVENT_MARKER_SET_COLOR = "modules_marker_setcolor";

class Marker {
	private markers = [];

	constructor() {
		Main.eventHandlers[EVENT_MARKER_CREATE] = this.markerCreateEvent;
		Main.eventHandlers[EVENT_MARKER_DESTROY] = this.markerDestroyEvent;
		Main.eventHandlers[EVENT_MARKER_SET_COLOR] = this.markerSetColorEvent;
	}

	private markerCreateEvent = (args: System.Array<any>) => {
		var markerID: number = args[0] as number;
		var markerType: number = args[1] as number;
		var markerPosition: Vector3 = args[2] as Vector3;
		var markerColorR: number = args[3] as number;
		var markerColorG: number = args[4] as number;
		var markerColorB: number = args[5] as number;
		var markerColorA: number = args[6] as number;

		if(this.markers[markerID] !== undefined) {
			if(this.markers[markerID] != null) {
				API.deleteEntity(this.markers[markerID]);
			}
		}

		this.markers[markerID] = API.createMarker(markerType, markerPosition, new Vector3(), new Vector3(), new Vector3(1, 1, 1),
			markerColorR, markerColorG, markerColorB, markerColorA);
	}

	private markerDestroyEvent = (args: System.Array<any>) => {
		var markerID: number = args[0] as number;

		if(this.markers[markerID] === undefined) return;
		if(this.markers[markerID] == null) return;

		API.deleteEntity(this.markers[markerID]);
	}

	private markerSetColorEvent = (args: System.Array<any>) => {
		var markerID: number = args[0] as number;

		if(this.markers[markerID] === undefined) return;
		if(this.markers[markerID] == null) return;

		var cA: number = args[1] as number;
		var cR: number = args[2] as number;
		var cG: number = args[3] as number;
		var cB: number = args[4] as number;

		API.setMarkerColor(this.markers[markerID], cA, cR, cG, cB);
	}
}