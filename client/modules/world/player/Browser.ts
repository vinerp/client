const EVENT_BROWSER_CREATE = "modules_player_browser_create";
const EVENT_BROWSER_DESTROY = "modules_player_browser_destroy";
const EVENT_BROWSER_SETURL = "modules_player_browser_seturl";
const EVENT_BROWSER_SHOW = "modules_player_browser_show";
const EVENT_BROWSER_HIDE = "modules_player_browser_hide";
const EVENT_BROWSER_SET_POSITION = "modules_player_browser_set_position";
const EVENT_BROWSER_SET_SIZE = "modules_player_browser_set_size";

class Browser {
	private browsers: CefHelper[] = [];

	constructor() {
		Main.eventHandlers[EVENT_BROWSER_CREATE] = this.browserCreateEvent;
		Main.eventHandlers[EVENT_BROWSER_DESTROY] = this.browserDestroyEvent;
		Main.eventHandlers[EVENT_BROWSER_SETURL] = this.browserSetUrlEvent;
		Main.eventHandlers[EVENT_BROWSER_SHOW] = this.browserShowEvent;
		Main.eventHandlers[EVENT_BROWSER_HIDE] = this.browserHideEvent;
		Main.eventHandlers[EVENT_BROWSER_SET_POSITION] = this.browserSetPositionEvent;
		Main.eventHandlers[EVENT_BROWSER_SET_SIZE] = this.browserSetSizeEvent;
	}

	findBrowserByName(name: string): CefHelper {
		var returnValue = null;
		this.browsers.forEach((val, index, array) => {
			if(val.Name == name) returnValue = val;
		});
		return returnValue;
	}

	findBrowserIndexByName(name: string): number {
		var browser = this.findBrowserByName(name);
		if(browser == null) return -1;

		return this.browsers.indexOf(browser);
	}

	private browserCreateEvent = (args: System.Array<any>) => {
        var browserName:string = args[0];
        var browserResolutionType:number = args[1];
        var browserWidth:number = args[2];
        var browserHeight:number = args[3];

        if(this.findBrowserByName(browserName) != null) {
        	this.findBrowserByName(browserName).destroy();
        }

        var createdBrowser = new CefHelper(browserName);

        if(browserResolutionType == 1)
	    {
	    	var fullResolution = API.getScreenResolution();
	    	browserWidth = fullResolution.Width;
	    	browserHeight = fullResolution.Height;
	    }

	    createdBrowser.create(browserWidth, browserHeight);
        this.browsers.push(createdBrowser);
    }

    private browserDestroyEvent = (args: System.Array<any>) => {
        var browserName:string = args[0];

        var browserIndex = this.findBrowserIndexByName(browserName);
        if(browserIndex == -1) {
        	API.sendChatMessage("Attempted to destroy a browser that does not exist. Name: " + browserName);
        	return;
        }
        this.browsers[browserIndex].destroy();
        this.browsers.splice(browserIndex, 1);
    }

    private browserSetUrlEvent = (args: System.Array<any>) => {
        var browserName:string = args[0];
        var browserPath:string = args[1];
        var enableCursor:boolean = args[2];
        var show:boolean = args[3];

        var browser = this.findBrowserByName(browserName);
        if(browser == null)
        {
        	API.sendChatMessage("SETURL: Browser with the name " + browserName + " was not found.");
        	return;
        }
        if(!browser.created)
        {
        	API.sendChatMessage("SETURL: Browser with the name " + browserName + " is not created.");
        	return;
        }

        browser.goto(browserPath);
        
        if(enableCursor) API.showCursor(true);
        if(show) browser.show();
    }

    private browserShowEvent = (args: System.Array<any>) => {
        var browserName:string = args[0];
        var enableCursor:boolean = args[1];
        
        var browser = this.findBrowserByName(browserName);
        if(browser == null)
        {
        	API.sendChatMessage("SHOW: Browser with the name " + browserName + " was not found.");
        	return;
        }
        if(!browser.created)
        {
        	API.sendChatMessage("SHOW: Browser with the name " + browserName + " is not created.");
        	return;
        }

        browser.show();
        if(enableCursor) API.showCursor(true);
    }

    private browserHideEvent = (args: System.Array<any>) => {
        var browserName:string = args[0];
        var hideCursor:boolean = args[1];

        var browser = this.findBrowserByName(browserName);
        if(browser == null)
        {
        	API.sendChatMessage("HIDE: Browser with the name " + browserName + " was not found.");
        	return;
        }
        if(!browser.created)
        {
        	API.sendChatMessage("HIDE: Browser with the name " + browserName + " is not created.");
        	return;
        }

        browser.hide();
        if(hideCursor) API.showCursor(false);
    }

    private browserSetPositionEvent = (args: System.Array<any>) => {
    	var browserName:string = args[0];
    	var positionX:number = args[1];
    	var positionY:number = args[2];

    	var browser = this.findBrowserByName(browserName);
        if(browser == null)
        {
        	API.sendChatMessage("SETPOSITION: Browser with the name " + browserName + " was not found.");
        	return;
        }
        if(!browser.created)
        {
        	API.sendChatMessage("SETPOSITION: Browser with the name " + browserName + " is not created.");
        	return;
        }

        browser.setPosition(positionX, positionY);
    }

    private browserSetSizeEvent = (args: System.Array<any>) => {
    	var browserName:string = args[0];
    	var width:number = args[1];
    	var height:number = args[2];

    	var browser = this.findBrowserByName(browserName);
        if(browser == null)
        {
        	API.sendChatMessage("SETSIZE: Browser with the name " + browserName + " was not found.");
        	return;
        }
        if(!browser.created)
        {
        	API.sendChatMessage("SETSIZE: Browser with the name " + browserName + " is not created.");
        	return;
        }

        browser.setSize(width, height);
    }
}