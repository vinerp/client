const EVENT_SCOREBOARD_PLAYER_ADD = "modules_player_scoreboard_player_add";
const EVENT_SCOREBOARD_PLAYER_REMOVE = "modules_player_scoreboard_player_remove";
const EVENT_SCOREBOARD_PLAYER_UPDATE = "modules_player_scoreboard_player_update";

class Scoreboard {
	constructor() {
		Main.eventHandlers[EVENT_SCOREBOARD_PLAYER_ADD] = this.scoreboardPlayerAddEvent;
        Main.eventHandlers[EVENT_SCOREBOARD_PLAYER_REMOVE] = this.scoreboardPlayerRemoveEvent;
        Main.eventHandlers[EVENT_SCOREBOARD_PLAYER_UPDATE] = this.scoreboardPlayerUpdateEvent;
	}

	private scoreboardPlayerAddEvent = (args: System.Array<any>) => {
        var scoreboardPlayerName: string = args[0] as string;
        var scoreboardPlayerPing: number = args[1] as number;
        var scoreboardPlayerID: number = args[2] as number;

        Main.browser.findBrowserByName("scoreboard").eval("addPlayer('" + scoreboardPlayerName + "', " + scoreboardPlayerPing +", " + scoreboardPlayerID + ");");
        //API.sendChatMessage("Player " + scoreboardPlayerName + " was added to your scoreboard");
    }

    private scoreboardPlayerRemoveEvent = (args: System.Array<any>) => {
        var scoreboardPlayerName: string = args[0] as string;

        //this.browsers[browserID].eval("removePlayer('" + scoreboardPlayerName + "');");
        Main.browser.findBrowserByName("scoreboard").eval("removePlayer('" + scoreboardPlayerName + "');");
    }

    private scoreboardPlayerUpdateEvent = (args: System.Array<any>) => {
        var scoreboardPlayerName: string = args[0] as string;
        var scoreboardPlayerPing: number = args[1] as number;

        //this.browsers[browserID].eval("setPing('" + scoreboardPlayerName + "', " + scoreboardPlayerPing +");");
        Main.browser.findBrowserByName("scoreboard").eval("setPing('" + scoreboardPlayerName + "', " + scoreboardPlayerPing +");");
    }
}