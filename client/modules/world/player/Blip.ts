const EVENT_BLIP_CREATE = "modules_blip_create";
const EVENT_BLIP_DESTROY = "modules_blip_destroy";
const EVENT_BLIP_SET_COLOR = "modules_blip_setcolor";
const EVENT_BLIP_SET_SPRITE = "modules_blip_setsprite";

class Blip {
	private blips = [];

	constructor() {
		Main.eventHandlers[EVENT_BLIP_CREATE] = this.blipCreateEvent;
		Main.eventHandlers[EVENT_BLIP_DESTROY] = this.blipDestroyEvent;
		Main.eventHandlers[EVENT_BLIP_SET_COLOR] = this.blipSetColorEvent;
		Main.eventHandlers[EVENT_BLIP_SET_SPRITE] = this.blipSetSpriteEvent;
	}

	private blipCreateEvent = (args: System.Array<any>) => {
		var blipID: number = args[0] as number;
		var blipPosition: Vector3 = args[1] as Vector3;
		var blipSprite: number = args[2] as number;
		var blipColor: number = args[3] as number;

		if(this.blips[blipID] !== undefined) {
			if(this.blips[blipID] != null) {
				API.deleteEntity(this.blips[blipID]);
			}
		}
		
		this.blips[blipID] = API.createBlip(blipPosition);

		API.setBlipSprite(this.blips[blipID], blipSprite);
		API.setBlipColor(this.blips[blipID], blipColor);
	}

	private blipDestroyEvent = (args: System.Array<any>) => {
		var blipID: number = args[0] as number;

		if(this.blips[blipID] === undefined) return;
		if(this.blips[blipID] == null) return;

		API.deleteEntity(this.blips[blipID]);
	}

	private blipSetColorEvent = (args: System.Array<any>) => {
		var blipID: number = args[0] as number;
		var blipColor: number = args[1] as number;

		if(this.blips[blipID] === undefined) return;
		if(this.blips[blipID] == null) return;
		API.setBlipColor(this.blips[blipID], blipColor);
	}

	private blipSetSpriteEvent = (args: System.Array<any>) => {
		var blipID: number = args[0] as number;
		var blipSprite: number = args[1] as number;

		if(this.blips[blipID] === undefined) return;
		if(this.blips[blipID] == null) return;
		API.setBlipSprite(this.blips[blipID], blipSprite);
	}
}