jQuery(function() {
    
});

function addPlayer(name, ping, id) {
	$('#player-holder').append("<tr id='player-" + name +"'><td>" + id + "</td><td>" + name + "</td><td id='player-" + name + "-ping'>" + ping + "</td></tr>");
}

function removePlayer(name) {
	if($('#player-' + name).length) {
		$('#player-' + name).remove();
	}
}

function setPing(name, ping) {
	if($('#player-' + name).length) {
		$('#player-' + name + '-ping').html(ping);
	}
}