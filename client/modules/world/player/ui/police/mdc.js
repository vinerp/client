var currentViewingPlayer = "";
var lastWarrantNumber = 0;
var chargesAdded = 0;

jQuery(function() {
    $("#tab-personal-info").click(function() {
    	hideAllTabs();
    	$('#content-personal-info').show();
    });

    $("#tab-vehicles").click(function() {
    	hideAllTabs();
    	$("#content-vehicles").show();
    });

    $("#tab-warrants").click(function() {
    	hideAllTabs();
    	$("#content-warrants").show();
    });

    $("#tab-criminal-record").click(function() {
    	hideAllTabs();
    	$("#content-criminal-record").show();
    });

    $("#button-search").click(function() {
    	var searchType = $('#search-type').val();
    	var searchValue = $('#search-value').val();
    	console.log('clicked button, search type: ' + searchType + ' and value ' + searchValue);
    	resourceEval("API.triggerServerEvent('modules_faction_police_mdc_search', " + searchType + ", '" + searchValue +"');");

    	clearPeopleSearchResults();
    });

    $("#button-close").click(function() {
    	resourceEval("Main.browser.findBrowserByName('main').hide(); API.showCursor(false);")
    });

    $("#button-add-warrant-view").click(function() {
    	hideAllTabs();
    	$("#content-add-warrant").show();
    });

    $("#button-submit-warrant").click(function() {
  		var warrantReason = $("#warrant-reason").val();
    	resourceEval("API.triggerServerEvent('modules_faction_police_mdc_add_warrant', '" + currentViewingPlayer + "', '" + warrantReason +"');");
    });

    $("#button-add-charge").click(function() {
    	hideAllTabs();
    	$("#content-criminal-record-add").show();
    	chargesAdded = 0;
    	resourceEval("API.setCanOpenChat(false);");
    });

    $("#button-criminal-record-add-charge").click(function() {
    	var chargeValue = $("#criminal-record-add-charge").val();

    	if(chargeValue.length == 0) {
    		hideAllTabs();
    		$("#content-criminal-record-add-incident-report").show();
    	} else {
    		resourceEval("API.triggerServerEvent('modules_faction_police_mdc_criminal_record_add_new_charge', '" + chargeValue +"', " + (chargesAdded++) + ");");
    		$("#criminal-record-add-charge").val("");
    	}
    });

    $("#button-criminal-record-add-incident-report").click(function() {
    	var incidentReportValue = $("#criminal-record-add-incident-report").val();

    	resourceEval("API.triggerServerEvent('modules_faction_police_mdc_criminal_record_add_new_incident_report', '" + incidentReportValue +"', '" + currentViewingPlayer + "');");
    	hideAllTabs();
    	$("#content-criminal-record").show();
    	resourceEval("API.setCanOpenChat(true);");
    	$("#criminal-record-add-incident-report").val("");
    });

    $("body").on("click", "a#person-view", function() {
    	var playerName = $(this).attr("player");
    	currentViewingPlayer = playerName;
    	resourceEval("API.triggerServerEvent('modules_faction_police_mdc_view_select', 0, '" + playerName + "');")
    });

    $("body").on("click", "a#vehicle-view", function() {
    	var vehiclePlate = $(this).attr("plate");
    	resourceEval("API.triggerServerEvent('modules_faction_police_mdc_view_select', 1, '" + vehiclePlate + "');")
    });

    $("body").on("click", "a#criminal-charge-view", function() {
    	var chargeId = $(this).attr("chargeid");
    	resourceEval("API.triggerServerEvent('modules_faction_police_mdc_criminal_charge_view', '" + currentViewingPlayer +"', '" + chargeId + "');");
    	$("#criminal-charge-charges").html("");
    });

    $("body").on("click", "a#criminal-charge-delete", function() {
    	var chargeId = $(this).attr("chargeid");
    	removeCriminalRecord(chargeId);
    	resourceEval("API.triggerServerEvent('modules_faction_police_mdc_criminal_charge_delete', '" + currentViewingPlayer +"', '" + chargeId + "');");
    });
});

function enableButtons() {
	$("#tab-personal-info").prop("disabled", false);
	$("#tab-vehicles").prop("disabled", false);
	$("#tab-warrants").prop("disabled", false);
	$("#tab-criminal-record").prop("disabled", false);
}

function clearPeopleSearchResults() {
	$("#table-people-holder").html(`
		<tr>
			<th>Name</th>
			<th>Phone #</th>
			<th>View</th>
		</tr>
	`);
}

function hideAllTabs() {
	$('#content-personal-info').hide();
	$("#content-vehicles").hide();
	$("#content-warrants").hide();
	$("#content-add-warrant").hide();
	$("#content-criminal-record").hide();
	$("#content-welcome").hide();
	$("#search-results-people").hide();
	$("#search-results-vehicles").hide();
	$("#content-not-found").hide();
	$("#content-player-no-longer-connected").hide();
	$("#content-criminal-record-view").hide();
	$("#content-criminal-record-add").hide();
	$("#content-criminal-record-add-incident-report").hide();
}

function showSearchNotFound() {
	hideAllTabs();
	$("#content-not-found").show();
}

function fillPlayerPersonalInfo(strObj) {
	var data = JSON.parse(strObj);

	$("#personal-info-name").text(data.Name);
	$("#personal-info-gender").text(data.Gender);
	$("#personal-info-phone-number").text(data.PhoneNumber);
	$("#personal-info-licenses").text(data.Licenses);
	$("#personal-info-number-of-owned-vehicles").text(data.NumberOfOwnedVehicles);
	$("#personal-info-active-warrant").text(data.ActiveWarrant);

	for(var i = 0; i < data.VehicleNames.length; i++) {
		addPlayerVehicle(data.VehicleNames[i], data.VehiclePlates[i]);
	}

	for(var i = 0; i < data.Warrants.length; i++) {
		addPlayerWarrant(i+1, data.Warrants[i]);
		lastWarrantNumber = i+1;
	}

	for(var i = 0; i < data.CriminalRecordDates.length; i++) {
		addCriminalRecord(data.CriminalRecordDates[i], data.CriminalRecordNumberOfCharges[i], data.CriminalRecordIds[i]);
	}
}

function fillCriminalCharge(strObj) {
	var data = JSON.parse(strObj);

	$("#criminal-charge-incident-report").text(data.IncidentReport);
	$("#criminal-charge-charged-by").text(data.ChargedBy + " (" + data.ChargerRank + ")");
	$("#criminal-charge-charge-date").text(data.ChargeDate);

	for(var i = 0; i < data.Charges.length; i++) {
		$("#criminal-charge-charges").append(`
			<li>` + data.Charges[i] + `</li>
		`);
	}
}

function showSearchResults(type)
{
	hideAllTabs();
	if(type == 0) {
		$("#search-results-people").show();
	} else if(type == 1) {
		$("#search-results-vehicles").show();
	}
}

function addVehicleResult(name, plate, owner/*, id*/) {
	$("#table-vehicles-holder").append(`
		<tr>
			<td>` + name + `</td>
			<td>` + plate + `</td>
			<td>` + owner + `</td>
			
		</tr>
	`);
	//<td><a href="#" id="vehicle-view" plate="` + plate +`">View</a></td>
}

function addPlayerVehicle(name, plate) {
	$("#player-vehicle-holder").append(`
		<tr>
			<td>` + name + `</td>
			<td>` + plate +`</td>
		</tr>
	`);
}

function addPlayerWarrant(number, reason) {
	$("#player-warrants-holder").append(`
		<tr>
			<td>` + number + `</td>
			<td>` + reason +`</td>
		</tr>
	`);
}

function addPersonResult(name, phoneNumber) {
	$("#table-people-holder").append(`
		<tr>
			<td>` + name + `</td>
			<td>` + phoneNumber + `</td>
			<td><a href="#" id="person-view" player="` + name + `">View</a></td>
		</tr>
	`);
}

function addCriminalRecord(date, numberOfCharges, id) {
	$("#player-criminal-record-holder").append(`
		<tr class="criminal-charge-row" chargeid="` + id + `">
			<td>` + date + `</td>
			<td>` + numberOfCharges + `</td>
			<td><a href="#" id="criminal-charge-view" chargeid="` + id + `">View</a></td>
			<td><a href="#" id="criminal-charge-delete" chargeid="` + id +`">Delete</a></td>
		</tr>
	`);
}

function removeCriminalRecord(id) {
	for(var i = 0; i < $(".criminal-charge-row").length; i++) {
		if($(".criminal-charge-row").eq(i).attr("chargeid") == id) {
			$(".criminal-charge-row").eq(i).remove();
		}
	}
}