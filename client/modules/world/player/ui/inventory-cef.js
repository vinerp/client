var vm = new Vue({
	el: '#app',
	data: function() {
		return {
			items: [
				{
					id: 5,
					name: "Test item name",
					quantity: 15
				},
				{
					id: 8,
					name: "Secnd item",
					quantity: 10
				}
			]
		};
	},
	mounted: function() {
		console.log('ready');
	}
});