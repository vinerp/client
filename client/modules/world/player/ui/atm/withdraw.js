var browserName = "main";

jQuery(function() {
    $('#closebutton').click(function() {
        resourceEval("Main.browser.findBrowserByName('" + browserName + "').hide(); API.showCursor(false);");
    });

    $('#depositbutton').click(function() {
        //resourceEval("API.triggerServerEvent(\"atm_deposit\");");
        var withdrawAmount = String($('#withdrawAmount').val());
        resourceEval("API.triggerServerEvent(\"modules_player_economy_bank_withdraw\", " + withdrawAmount + ");");
        resourceEval("Main.browser.findBrowserByName('" + browserName + "').hide(); API.showCursor(false);");
    });
});