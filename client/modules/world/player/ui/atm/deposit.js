var browserName = "main";

jQuery(function() {
    $('#closebutton').click(function() {
        resourceEval("Main.browser.findBrowserByName('" + browserName + "').hide(); API.showCursor(false);");
    });

    $('#depositbutton').click(function() {
        var depositAmount = String($('#depositAmount').val());
        resourceEval("API.triggerServerEvent(\"modules_player_economy_bank_deposit\", " + depositAmount + ");");
        resourceEval("Main.browser.findBrowserByName('" + browserName + "').hide(); API.showCursor(false);");
    });
});