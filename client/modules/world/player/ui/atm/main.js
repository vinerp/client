var browserName = "main";

jQuery(function() {
    $('#closebutton').click(function() {
        resourceEval("Main.browser.findBrowserByName('" + browserName + "').hide(); API.showCursor(false);");
        resourceEval("API.sendChatMessage(\"clicked close....\");");
    });

    /*$('#depositbutton').click(function() {
        resourceEval("API.triggerServerEvent(\"modules_player_ui_atm_show_deposit\");");
    });*/

    $('#withdrawbutton').click(function() {
        resourceEval("API.triggerServerEvent(\"modules_player_ui_atm_show_withdraw\");");
    });

    $('#checkbalancebutton').click(function() {
        resourceEval("API.triggerServerEvent(\"modules_player_economy_bank_checkbalance\");");
        resourceEval("Main.browser.findBrowserByName('" + browserName + "').hide(); API.showCursor(false);");
    });
});