const EVENT_MENU_CREATE = "modules_menu_show";
const EVENT_MENU_ONITEMSELECT = "modules_menu_onitemselect";
const EVENT_MENU_ONCLOSE = "modules_menu_onclose";
const EVENT_MENU_CLOSE = "modules_menu_close";

class Menu {
	private currentMenu: NativeUI.UIMenu;

	constructor() {
		this.currentMenu = null;
		Main.eventHandlers[EVENT_MENU_CREATE] = this.menuCreateEvent;
		Main.eventHandlers[EVENT_MENU_CLOSE] = this.menuCloseEvent;

		API.onUpdate.connect(this.onUpdate);
	}

	private menuCreateEvent = (args: System.Array<any>) => {
		if(this.currentMenu != null) {
			this.currentMenu.Visible = false;
			this.currentMenu = null;
		}

		var banner: string = args[0] as string;
		var title: string = args[1] as string;
		var offsetX: number = args[2] as number;
		var offsetY: number = args[3] as number;
		var anchor: number = args[4] as number;
		var canExit: boolean = args[5] as boolean;
		var itemLabels: System.Array<string> = args[6] as System.Array<string>;
		var itemDescriptions: System.Array<string> = args[7] as System.Array<string>;

		this.currentMenu = API.createMenu(banner, title, offsetX, offsetY, anchor);


		/*if(!canExit) {
			var MenuControls: any;
			this.currentMenu.ResetKey(MenuControls.Back);
		}*/

		for(var i = 0; i < itemLabels.Count; i++) {
			this.currentMenu.AddItem(API.createMenuItem(itemLabels[i], itemDescriptions[i]));
		}

		this.currentMenu.RefreshIndex();

		this.currentMenu.OnItemSelect.connect(this.onItemSelect);
		this.currentMenu.OnMenuClose.connect(this.onMenuClose);

		this.currentMenu.Visible = true;
	}

	private menuCloseEvent = (args: System.Array<any>) => {
		if(this.currentMenu != null) {
			this.currentMenu.Visible = false;
			this.currentMenu = null;
		}
	}

	private onUpdate = () => {
		if(this.currentMenu != null) {
			API.drawMenu(this.currentMenu);
		}
	}

	private onItemSelect = (sender: NativeUI.UIMenu, item: NativeUI.UIMenuItem, index: number) => {
		API.triggerServerEvent(EVENT_MENU_ONITEMSELECT, index);
		this.currentMenu.Visible = false;
		this.currentMenu = null;
	}

	private onMenuClose = (sender: NativeUI.UIMenu) => {
		API.triggerServerEvent(EVENT_MENU_ONCLOSE);
	}
}