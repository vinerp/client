const EVENT_PLAYER_MONEY_UPDATE = "modules_player_money_update";
const EVENT_PLAYER_DISABLE_CHAT = "modules_player_disable_chat";
const EVENT_PLAYER_ENABLE_CHAT = "modules_player_enable_chat";
const EVENT_PLAYER_AUTH_START_CAMERA = "modules_player_start_auth_camera";
const EVENT_PLAYER_AUTH_RESET_CAMERA = "modules_player_reset_auth_camera";
const EVENT_PLAYER_ONKEYDOWN = "modules_player_event_onkeydown";
const EVENT_PLAYER_READY_HEARTBEAT = "modules_players_event_ready_heartbeat";
const EVENT_PLAYER_CUSTOMIZATION_CAMERA = "modules_players_event_customization_camera";
const EVENT_PLAYER_CUSTOMIZATION_CAMERA_END = "modules_players_event_customization_camera_end";
const EVENT_PLAYER_GROUND_HEIGHT = "modules_players_event_ground_height";
const EVENT_PLAYER_TOGGLE_AIRBRK = "modules_players_event_toggle_airbrk";
const EVENT_PLAYER_ATTEMPT_ENTRANCE = "modules_players_event_attempt_entrance";

class Player {
    private currentMoney: number = 0;
    private selectingObject: boolean = false;
    private sentReadyHeartbeat: boolean = false;
    private lastTime: number = 0;

    private airbrk: boolean = false;

    private authFromCamera: GTANetwork.Javascript.GlobalCamera;
    private authToCamera: GTANetwork.Javascript.GlobalCamera;
    private customizationCamera: GTANetwork.Javascript.GlobalCamera;

    constructor() {
        Main.eventHandlers[EVENT_PLAYER_MONEY_UPDATE] = this.moneyUpdateEvent;
        Main.eventHandlers[EVENT_PLAYER_DISABLE_CHAT] = this.disableChatEvent;
        Main.eventHandlers[EVENT_PLAYER_ENABLE_CHAT] = this.enableChatEvent;
        Main.eventHandlers[EVENT_PLAYER_AUTH_START_CAMERA] = this.authStartCameraEvent;
        Main.eventHandlers[EVENT_PLAYER_AUTH_RESET_CAMERA] = this.authResetCameraEvent;
        Main.eventHandlers[EVENT_PLAYER_CUSTOMIZATION_CAMERA] = this.customizationCameraEvent;
        Main.eventHandlers[EVENT_PLAYER_CUSTOMIZATION_CAMERA_END] = this.customizationCameraEndEvent;
        Main.eventHandlers[EVENT_PLAYER_GROUND_HEIGHT] = this.onGroundHeight;
        Main.eventHandlers[EVENT_PLAYER_TOGGLE_AIRBRK] = this.onToggleAirbrk;

        Main.eventHandlers["select_object"] = (args: System.Array<any>) => {
            this.selectingObject = true;
            API.showCursor(true);
        }

        API.onUpdate.connect(this.onUpdate);
        API.onKeyDown.connect(this.onKeyDown);
        API.onKeyUp.connect(this.onKeyUp);
    }

    private onKeyDown = (sender: any, e: System.Windows.Forms.KeyEventArgs) => {
        //API.triggerServerEvent(EVENT_PLAYER_ONKEYDOWN, e.KeyValue, API.isChatOpen(), e.Control, e.Shift);
        
    }

    private onKeyUp = (sender: any, e: System.Windows.Forms.KeyEventArgs) => {
        if(this.airbrk) {
            if(e.KeyCode == Keys.W) {
                var forwardPosition = VMath.getLocalPlayerForwardPosition(0.5);
                API.setEntityPosition(API.getLocalPlayer(), forwardPosition);
            }
        }

        if(!API.isChatOpen()) {
            if(!API.isPlayerInAnyVehicle(API.getLocalPlayer())) {
                if(e.KeyCode == Keys.Enter) {
                    API.triggerServerEvent(EVENT_PLAYER_ATTEMPT_ENTRANCE);
                }
            }
        }

        if(e.KeyCode == Keys.F2) {
            API.showCursor(!API.isCursorShown());
        }
    }

    private onUpdate = () => {

        API.drawText("$" + this.currentMoney, 1820, 100, 0.6, 0, 255, 0, 255, 0, 2, true, true, 0);

        if(!this.sentReadyHeartbeat) {
            API.triggerServerEvent(EVENT_PLAYER_READY_HEARTBEAT);
            this.sentReadyHeartbeat = true;
        }

        if(this.selectingObject)
        {
            var cursorPos = API.getCursorPositionMantainRatio();
            var screenToWorld = API.screenToWorldMantainRatio(cursorPos);
            var rayCast = API.createRaycast(API.getGameplayCamPos(), screenToWorld, Enums.IntersectOptions.Everything, null);

            if(rayCast.didHitEntity) {
                var hitEntity = rayCast.hitEntity;
                API.setEntityTransparency(hitEntity, 100);
                API.displaySubtitle("Hit entity " + hitEntity.Value);
            }

            if(API.isDisabledControlPressed(Enums.Controls.Attack)) {
                API.showCursor(false);
                this.selectingObject = false;
            }
        }
    }

    private moneyUpdateEvent = (args: System.Array<any>) => {
        this.currentMoney = args[0];
    }

    private disableChatEvent = (args: System.Array<any>) => {
        API.setCanOpenChat(false);
    }

    private enableChatEvent = (args: System.Array<any>) => {
        API.setCanOpenChat(true);
    }

    private authStartCameraEvent = (args: System.Array<any>) => {
        API.setHudVisible(false);
        this.authFromCamera = API.createCamera(new Vector3(431.8062, 845.4288, 233.92), new Vector3(-3.549473, -1.086838, 159.0152));
        this.authToCamera = API.createCamera(new Vector3(-134.3234, -746.7285, 89.73032), new Vector3(0.4361472, -3.680317, 153.9228));
        API.setActiveCamera(this.authFromCamera);
        API.interpolateCameras(this.authFromCamera, this.authToCamera, 120 * 1000, true, true);
    }

    private authResetCameraEvent = (args: System.Array<any>) => {
        API.setHudVisible(true);
        API.setGameplayCameraActive();
        API.detachCamera(this.authFromCamera);
        API.detachCamera(this.authToCamera);
    }  

    private customizationCameraEvent = (args: System.Array<any>) => {
        var position: Vector3 = args[0];
        var pointAt: Vector3 = args[1];

        var fixCamera = API.createCamera(position.Subtract(new Vector3(5, 0, 0)), new Vector3());
        API.pointCameraAtEntity(fixCamera, API.getLocalPlayer(), new Vector3());
        API.setActiveCamera(fixCamera);
        //API.setGameplayCameraActive();
        //API.sleep(200);

        this.customizationCamera = API.createCamera(position, new Vector3());
        API.pointCameraAtPosition(this.customizationCamera, pointAt);

        //API.setActiveCamera(this.customizationCamera);
    }

    private customizationCameraEndEvent = (args: System.Array<any>) => {
        API.detachCamera(this.customizationCamera);
        API.setGameplayCameraActive();
    }

    private onGroundHeight = (args: System.Array<any>) => {
        
    }

    private onToggleAirbrk = (args: System.Array<any>) => {
         var toggleVal = args[0] as boolean;

         this.airbrk = toggleVal;

         API.setEntityCollissionless(API.getLocalPlayer(), toggleVal);
         //API.(API.getLocalPlayer(), toggleVal);
    }    
}