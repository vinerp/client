var personalVehicleBlip = null;
var dealershipMenu = null;

API.onServerEventTrigger.connect(function(eventName, args) {
	if(eventName == "modules_vehicle_personalvehicle_blip_show") {
		if(personalVehicleBlip != null) API.deleteEntity(personalVehicleBlip);
		personalVehicleBlip = API.createBlip(args[0]);
		API.setBlipSprite(personalVehicleBlip, 225);
		API.setBlipColor(personalVehicleBlip, 1); // 1 == red
	} else if(eventName == "modules_vehicle_dealership_menu_show") {
		
		dealershipMenu = API.createMenu("Dealership", 0, 0, 6);

		dealershipMenu.OnItemSelect.connect(function(sender, item, index) {
			// Item returns NativeUI.UIMenuItem
			API.showCursor(false);
			dealershipMenu.Visible = false;
			API.sendChatMessage("Selected item is " + API.toString(item));
			API.sendChatMessage("The selected index is " + index);

			/* Ignoring the "exit" option */
			if(index > 0)
			{
				API.triggerServerEvent("modules_vehicle_dealership_buy_select", index-1);
			}
		});

		API.sendNotification("Showing the menu name " + args[0]);
		
		dealershipMenu.ResetKey(menuControl.Back);
		API.setMenuSubtitle(dealershipMenu, args[0]);

		API.sendNotification("The length is " + args[1].Count);

		dealershipMenu.AddItem(API.createMenuItem("Exit"));

		for(var i = 0; i < args[1].Count; i++) {
			dealershipMenu.AddItem(API.createMenuItem("" + args[1][i] + " - $" + args[2][i], "Description"));
		}

		dealershipMenu.Visible = true;
		API.showCursor(true);
	}
});

API.onUpdate.connect(function() {
	if(dealershipMenu != null)
	{
		API.drawMenu(dealershipMenu);
	}
});
