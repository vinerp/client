const EVENT_DEALERSHIP_SHOW_MENU = "modules_vehicle_dealership_menu_show";
const EVENT_DEALERSHIP_SHOW_BUY_MENU = "modules_vehicle_dealership_buy_menu_show";
const EVENT_DEALERSHIP_BUY_SELECT = "modules_vehicle_dealership_buy_select";
const EVENT_DEALERSHIP_BUY_MENU_PURCHASE = "modules_vehicle_dealership_buy_menu_purchase";

class VehicleDealership {
	private dealershipMenu: NativeUI.UIMenu = null;
	private lastDealershipMenuId: number = 0;

	private dealershipBuyMenu: NativeUI.UIMenu = null;
	private lastDealershipBuyMenuId: number = 0;
	private lastDealershipBuyMenuVehicleIndex: number = 0;

	constructor() {
		Main.eventHandlers[EVENT_DEALERSHIP_SHOW_MENU] = this.showDealershipMenuEvent;
		Main.eventHandlers[EVENT_DEALERSHIP_SHOW_BUY_MENU] = this.showDealershipBuyMenuEvent;

		API.onUpdate.connect(this.onUpdate);
	}

	private onUpdate = () => {
		if(this.dealershipMenu != null) API.drawMenu(this.dealershipMenu);
		else if(this.dealershipBuyMenu != null) API.drawMenu(this.dealershipBuyMenu);
	}

	private showDealershipMenuEvent = (args: System.Array<any>) => {
		this.dealershipMenu = API.createMenu("Dealership", 0, 0, 6);

		this.lastDealershipMenuId = args[3];

		this.dealershipMenu.OnItemSelect.connect(this.onDealershipMenuItemSelect);

		this.dealershipMenu.ResetKey(NativeUI.UIMenu_MenuControls.Back);
		API.setMenuSubtitle(this.dealershipMenu, args[0]);
		this.dealershipMenu.AddItem(API.createMenuItem("Exit", ""));

		for(var i = 0; i < args[1].Count; i++)
		{
			this.dealershipMenu.AddItem(API.createMenuItem("" + args[1][i] + " - $" + args[2][i], ""));
		}

		this.dealershipMenu.Visible = true;
		API.showCursor(true);
	}

	private onDealershipMenuItemSelect = (sender: NativeUI.UIMenu, item: NativeUI.UIMenuItem, index: number) => {
		API.showCursor(false);
		this.dealershipMenu.Visible = false;
		if(index > 0)
		{
			API.triggerServerEvent(EVENT_DEALERSHIP_BUY_SELECT, index-1, this.lastDealershipMenuId)
		}
	}

	private showDealershipBuyMenuEvent = (args: System.Array<any>) => {
		/* Args:
           0 - ID of the dealership
           1 - The name of the vehicle
           2 - The price of the vehicle
           3 - the index of the vehicle */

		this.dealershipBuyMenu = API.createMenu("" + args[1], 0, 0, 6);

		this.lastDealershipBuyMenuId = args[0];
		this.lastDealershipBuyMenuVehicleIndex = args[3];

		this.dealershipBuyMenu.OnItemSelect.connect(this.onDealershipBuyMenuItemSelect);

		this.dealershipBuyMenu.ResetKey(NativeUI.UIMenu_MenuControls.Back);

		this.dealershipBuyMenu.AddItem(API.createMenuItem("Exit", "Exit the dealership menu"));
		this.dealershipBuyMenu.AddItem(API.createMenuItem("Purchase - $" + args[2], "Purchase the vehicle"));
		this.dealershipBuyMenu.Visible = true;
		API.showCursor(true);
	}

	private onDealershipBuyMenuItemSelect = (sender: NativeUI.UIMenu, item: NativeUI.UIMenuItem, index: number) => {
		API.showCursor(false);
		this.dealershipBuyMenu.Visible = false;

		if(index == 1)
		{
			API.triggerServerEvent(EVENT_DEALERSHIP_BUY_MENU_PURCHASE, this.lastDealershipBuyMenuId, this.lastDealershipBuyMenuVehicleIndex);
		}
	}
}

// let personalVehicleBlip : GTANetwork.Util.LocalHandle = null;
// let dealershipMenu : NativeUI.UIMenu = null;
// let dealershipBuyMenu : NativeUI.UIMenu = null;

// // Temp. workaround, GTA:N enums don't work with TS yet
// declare var menuControl:any;

// API.onServerEventTrigger.connect((eventName, args) => {
//     if(eventName == "modules_vehicle_personalvehicle_blip_show") {
//         if(personalVehicleBlip != null) API.deleteEntity(personalVehicleBlip);

//         personalVehicleBlip = API.createBlip(args[0] as Vector3);
//         API.setBlipSprite(personalVehicleBlip, 255);
//         API.setBlipColor(personalVehicleBlip, 1); // 1 == red

//     }if(eventName == "modules_vehicle_dealership_buy_menu_show") {
//         /* Args:
//             0 - ID of the dealership
//             1 - The name of the vehicle
//             2 - The price of the vehicle
//             3 - the index of the vehicle
//         */
//         dealershipBuyMenu = API.createMenu("" + args[1], 0, 0, 6);

//         dealershipBuyMenu.OnItemSelect.connect((sender, item, index) => {
//             API.showCursor(false);
//             dealershipBuyMenu.Visible = false;

//             if(index == 1) {
//                 API.triggerServerEvent("modules_vehicle_dealership_buy_menu_purchase", args[0], args[3]);
//             }
//         });

//         dealershipBuyMenu.ResetKey(menuControl.Back);
        
//         dealershipBuyMenu.AddItem(API.createMenuItem("Exit", "Exit this dealership menu"));
//         dealershipBuyMenu.AddItem(API.createMenuItem("Purchase - $" + args[2], "Purchase the vehicle"));
//         dealershipBuyMenu.Visible = true;
//         API.showCursor(true);
//     }
// });

// API.onUpdate.connect(() => {
//     if(dealershipMenu != null) {
//         API.drawMenu(dealershipMenu);
//     }
//     if(dealershipBuyMenu != null) {
//         API.drawMenu(dealershipBuyMenu);
//     }
// });