const EVENT_SHOW_PERSONALVEHICLE_BLIP = "modules_vehicle_personalvehicle_blip_show";
const EVENT_TRIGGER_VEHICLE_INDICATORS = "modules_vehicle_trigger_indicator";

class Vehicle
{
	private personalVehicleBlip: LocalHandle;

	constructor() {
		Main.eventHandlers[EVENT_SHOW_PERSONALVEHICLE_BLIP] = this.showPersonalVehicleBlipEvent;
		API.onKeyDown.connect(this.onKeyDown);
	}

	private showPersonalVehicleBlipEvent = (args: System.Array<any>) => {
		if(this.personalVehicleBlip != null) API.deleteEntity(this.personalVehicleBlip);

		this.personalVehicleBlip = API.createBlip(args[0] as Vector3);
        API.setBlipSprite(this.personalVehicleBlip, 255);
        API.setBlipColor(this.personalVehicleBlip, 1);
	}

	private onKeyDown = (sender: any, e: System.Windows.Forms.KeyEventArgs) => {
		if(!API.isChatOpen())
		{
			if(e.KeyValue == 188 || e.KeyValue == 190) {
				if(API.isPlayerInAnyVehicle(API.getLocalPlayer())) {
					API.triggerServerEvent(EVENT_TRIGGER_VEHICLE_INDICATORS, e.KeyValue);
				}
			}
		}
	}
}