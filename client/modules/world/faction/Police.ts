const EVENT_MDC_POPULATE_SEARCH_RESULTS = "modules_faction_police_mdc_populate_results";
const EVENT_MDC_SEARCH_NOT_FOUND = "modules_faction_police_mdc_search_not_found";
const EVENT_MDC_VIEW = "modules_faction_police_mdc_view";
const EVENT_MDC_WARRANT_ADDED = "modules_faction_police_mdc_warrant_added";
const EVENT_MDC_PLAYER_NO_LONGER_CONNECTED = "modules_faction_police_mdc_player_no_longer_connected";
const EVENT_MDC_CRIMINAL_CHARGE_SHOW = "modules_faction_police_mdc_criminal_charge_show";
const EVENT_MDC_CRIMINAL_RECORD_UPDATE = "modules_faction_police_mdc_criminal_record_update";

class FactionPolice {
	constructor() {
		Main.eventHandlers[EVENT_MDC_POPULATE_SEARCH_RESULTS] = this.onPopulateSearchResults;
		Main.eventHandlers[EVENT_MDC_SEARCH_NOT_FOUND] = this.onSearchNotFound;
		Main.eventHandlers[EVENT_MDC_VIEW] = this.onView;
		Main.eventHandlers[EVENT_MDC_WARRANT_ADDED] = this.onWarrantAdded;
		Main.eventHandlers[EVENT_MDC_PLAYER_NO_LONGER_CONNECTED] = this.onPlayerNoLongerConnected;
		Main.eventHandlers[EVENT_MDC_CRIMINAL_CHARGE_SHOW] = this.onCriminalChargeShow;
		Main.eventHandlers[EVENT_MDC_CRIMINAL_RECORD_UPDATE] = this.onCriminalRecordUpdate;
	}

	private onPopulateSearchResults = (args: System.Array<any>) => {
		API.sendChatMessage("populating search results");
		var populateType = args[0] as number;

		/* Populate players */
		if(populateType == 0) {
			var playerNameArray: System.Array<string> = args[1] as System.Array<string>;
			var playerPhoneNumberArray = args[2] as System.Array<string>;

			for (var i = 0; i < playerNameArray.Count; i++) {
				Main.browser.findBrowserByName("main").eval("addPersonResult('" + playerNameArray[i] + "', '" + playerPhoneNumberArray[i] + "')");
			}
			
			Main.browser.findBrowserByName("main").eval("showSearchResults(0)");
		} else if(populateType == 1) {

			var vehicleNamesArray: System.Array<string> = args[1];
			var vehiclePlatesArray: System.Array<string> = args[2];
			var vehicleOwnersArray: System.Array<string> = args[3];

			for (var i = 0; i < vehicleNamesArray.Count; i++) {
				Main.browser.findBrowserByName("main").eval("addVehicleResult('" + vehicleNamesArray[i] + "', '" + vehiclePlatesArray[i] + "', '" + vehicleOwnersArray[i] + "')");
			}

			Main.browser.findBrowserByName("main").eval("showSearchResults(1)");
		}
	}

	private onSearchNotFound = (args: System.Array<any>) => {
		Main.browser.findBrowserByName("main").eval("showSearchNotFound()");
	}

	private onView = (args: System.Array<any>) => {
		var serializedData = args[0];
		Main.browser.findBrowserByName("main").eval("fillPlayerPersonalInfo('" + serializedData + "')");
		Main.browser.findBrowserByName("main").eval("hideAllTabs(); $('#content-personal-info').show(); enableButtons();");
	}

	private onWarrantAdded = (args: System.Array<any>) => {
		/* Add the warrant to browser, show warrants tab */
		var reason: string = args[0];
		Main.browser.findBrowserByName("main").eval("addPlayerWarrant(++lastWarrantNumber, '" + reason +"'); hideAllTabs(); $('#content-warrants').show();");
	}

	private onPlayerNoLongerConnected = (args: System.Array<any>) => {
		Main.browser.findBrowserByName("main").eval("hideAllTabs(); $('#content-player-no-longer-connected').show();");
	}

	private onCriminalChargeShow = (args: System.Array<any>) => {
		var serializedData = args[0];
		Main.browser.findBrowserByName("main").eval("fillCriminalCharge('" + serializedData + "');");
		Main.browser.findBrowserByName("main").eval("hideAllTabs(); $('#content-criminal-record-view').show();");
	}

	private onCriminalRecordUpdate = (args: System.Array<any>) => {
		var chargeDate = args[0];
		var chargeCount = args[1];
		var chargeId = args[2];
		Main.browser.findBrowserByName("main").eval("addCriminalRecord('" + chargeDate + "', " + chargeCount + ", " + chargeId + ");");
	}
}