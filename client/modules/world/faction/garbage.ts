const EVENT_FACTION_GARBAGE_START = "modules_faction_garbage_job_start";
const EVENT_FACTION_GARBAGE_UPDATE = "modules_faction_garbage_job_update";
const EVENT_FACTION_GARBAGE_VEHICLE_ENTER = "modules_faction_garbage_vehicle_enter";
const EVENT_FACTION_GARBAGE_FINISH = "modules_faction_garbage_job_finish";
const EVENT_FACTION_GARBAGE_SET_BASE_BLIP = "modules_faction_garbage_set_base_blip";

class FactionGarbage {
    private maxTruckGarbage: number = 0;
    private currentTruckGarbage: number = 0;
    private inGarbageTruck: boolean = false;

    constructor() {
        Main.eventHandlers[EVENT_FACTION_GARBAGE_START] = this.jobStartEvent;
        Main.eventHandlers[EVENT_FACTION_GARBAGE_UPDATE] = this.jobUpdateEvent;
        Main.eventHandlers[EVENT_FACTION_GARBAGE_VEHICLE_ENTER] = this.vehicleEnterEvent;
        Main.eventHandlers[EVENT_FACTION_GARBAGE_FINISH] = this.jobFinishEvent;
        Main.eventHandlers[EVENT_FACTION_GARBAGE_SET_BASE_BLIP] = this.setBaseBlipEvent;

        API.onPlayerExitVehicle.connect(this.playerExitVehicle);
        API.onUpdate.connect(this.onUpdate);
    }

    private jobStartEvent = (args: System.Array<any>) => {
        /**
         * Args:
         * 0 - current garbage
         * 1 - max garbage
         */

        this.currentTruckGarbage = args[0];
        this.maxTruckGarbage = args[1];

        API.sendChatMessage("Starting the job");
    }

    private jobUpdateEvent = (args: System.Array<any>) => {
        /**
         * Args:
         * 0 - current garbage
         */

        this.currentTruckGarbage = args[0];

        API.sendChatMessage("Updated garbage");
    }

    private vehicleEnterEvent = (args: System.Array<any>) => {
        this.inGarbageTruck = true;
        this.currentTruckGarbage = args[0];
        this.maxTruckGarbage = args[1];
    }

    private jobFinishEvent = (args: System.Array<any>) => {
        if(this.inGarbageTruck) this.inGarbageTruck = false;
    }

    private setBaseBlipEvent = (args: System.Array<any>) => {
        /*this.garbageBaseBlip = API.createBlip(new Vector3(args[0], args[1], args[2]));
        API.setBlipSprite(this.garbageBaseBlip, 1);
        API.setBlipColor(this.garbageBaseBlip, 64);*/
    }

    private playerExitVehicle = (entity: LocalHandle) => {
        if(this.inGarbageTruck) this.inGarbageTruck = false;
    }

    private onUpdate = () => {
        if(this.inGarbageTruck)
        {
            API.drawText("Current garbage: " + this.currentTruckGarbage + "/" + this.maxTruckGarbage, 300, 770, 0.5, 255, 0, 0, 255, 0, 0, true, false, 0);
        }
    }
}

/*var currentGarbageMarker : GTANetwork.Util.LocalHandle = null;
var currentGarbageBlip : GTANetwork.Util.LocalHandle = null;
var garbageBaseBlip : GTANetwork.Util.LocalHandle = null;
var inGarbageTruck : boolean = false;
var resolution : Size = null;
var currentGarbage : number = 0;
var maxGarbage : number = 0;

API.onServerEventTrigger.connect((eventName, args) => {

    if(eventName == "modules_faction_garbage_set_base_blip") {
        if(garbageBaseBlip == null) {
            garbageBaseBlip = API.createBlip(new Vector3(args[0], args[1], args[2]));
            API.setBlipSprite(garbageBaseBlip, 1);
            API.setBlipColor(garbageBaseBlip, 64);
        }
    }
});

API.onUpdate.connect(() => {
    if(inGarbageTruck)
    {
        API.drawText("Current garbage: " + currentGarbage + "/" + maxGarbage, 300, resolution.Height + 50, 0.5, 255, 0, 0, 255, 0, 0, true, false, 0);
    }
});*/