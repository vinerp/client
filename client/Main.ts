/// <reference path="../types-gtanetwork/index.d.ts" />


class Main {
	public static eventHandlers : { [key: string]: (args: System.Array<any>) => void} = {};

	static player: Player;
	static browser: Browser;

	constructor() {
		API.onServerEventTrigger.connect(this.onEventTrigger);

		Main.player = new Player();
		Main.browser = new Browser();
		new Vehicle();
		new FactionGarbage();
		new FactionPolice();
		new VehicleDealership();
		new Menu();
		new Blip();
		new Marker();
		new Scoreboard();
	}

	private onEventTrigger = (eventName: string, args: System.Array<any>) => {
		//API.sendChatMessage("Debug: event " + eventName);

		var handler = Main.eventHandlers[eventName];
		if(typeof(handler) == undefined || handler == null) {
			API.sendChatMessage("Error: no handler for event " + eventName);
			return;
		}

		handler.call(this, args);
	};
}

API.onResourceStart.connect(() => {
	new Main();
});