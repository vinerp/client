class Main {
    constructor() {
        this.onEventTrigger = (eventName, args) => {
            var handler = Main.eventHandlers[eventName];
            if (typeof (handler) == undefined || handler == null) {
                API.sendChatMessage("Error: no handler for event " + eventName);
                return;
            }
            handler.call(this, args);
        };
        API.onServerEventTrigger.connect(this.onEventTrigger);
        Main.player = new Player();
        Main.browser = new Browser();
        new Vehicle();
        new FactionGarbage();
        new FactionPolice();
        new VehicleDealership();
        new Menu();
        new Blip();
        new Marker();
        new Scoreboard();
    }
}
Main.eventHandlers = {};
API.onResourceStart.connect(() => {
    new Main();
});
API.onServerEventTrigger.connect((eventName, args) => {
    if (eventName == "ui_auth_prompt_login_show") {
        API.sendNotification("Showing notification");
    }
});
class CefHelper {
    constructor(name) {
        this.created = false;
        this.Name = name;
    }
    create(width = 1920, height = 1080) {
        if (!this.created) {
            this.created = true;
            var resolution = API.getScreenResolution();
            this.browser = API.createCefBrowser(resolution.Width, resolution.Height, true);
            API.waitUntilCefBrowserInit(this.browser);
            API.setCefBrowserPosition(this.browser, 0, 0);
            API.setCefBrowserHeadless(this.browser, true);
        }
    }
    setPosition(x, y) {
        if (!this.created)
            return;
        API.setCefBrowserPosition(this.browser, x, y);
    }
    setSize(width, height) {
        if (!this.created)
            return;
        API.setCefBrowserSize(this.browser, width, height);
    }
    hide() {
        if (!this.created)
            return;
        if (!API.getCefBrowserHeadless(this.browser)) {
            API.setCefBrowserHeadless(this.browser, true);
        }
    }
    show() {
        if (!this.created)
            return;
        if (API.getCefBrowserHeadless(this.browser)) {
            API.setCefBrowserHeadless(this.browser, false);
        }
    }
    goto(path) {
        API.loadPageCefBrowser(this.browser, path);
    }
    destroy() {
        this.created = false;
        API.destroyCefBrowser(this.browser);
        API.showCursor(false);
    }
    eval(string) {
        if (!this.created)
            return;
        this.browser.eval(string);
    }
    call(method, ...args) {
        if (!this.created)
            return;
        this.browser.call(method, args);
    }
}
class VMath {
    static getLocalPlayerForwardPosition(range) {
        var position = API.getEntityPosition(API.getLocalPlayer());
        var rotation = API.getEntityRotation(API.getLocalPlayer());
        var angle = VMath.clampAngle(rotation.Z) * (Math.PI / 180);
        position.X += range * Math.sin(-angle);
        position.Z += range * Math.cos(-angle);
        return position;
    }
    static clampAngle(angle) {
        return (angle + Math.ceil(-angle / 360) * 360);
    }
}
const EVENT_FACTION_GARBAGE_START = "modules_faction_garbage_job_start";
const EVENT_FACTION_GARBAGE_UPDATE = "modules_faction_garbage_job_update";
const EVENT_FACTION_GARBAGE_VEHICLE_ENTER = "modules_faction_garbage_vehicle_enter";
const EVENT_FACTION_GARBAGE_FINISH = "modules_faction_garbage_job_finish";
const EVENT_FACTION_GARBAGE_SET_BASE_BLIP = "modules_faction_garbage_set_base_blip";
class FactionGarbage {
    constructor() {
        this.maxTruckGarbage = 0;
        this.currentTruckGarbage = 0;
        this.inGarbageTruck = false;
        this.jobStartEvent = (args) => {
            this.currentTruckGarbage = args[0];
            this.maxTruckGarbage = args[1];
            API.sendChatMessage("Starting the job");
        };
        this.jobUpdateEvent = (args) => {
            this.currentTruckGarbage = args[0];
            API.sendChatMessage("Updated garbage");
        };
        this.vehicleEnterEvent = (args) => {
            this.inGarbageTruck = true;
            this.currentTruckGarbage = args[0];
            this.maxTruckGarbage = args[1];
        };
        this.jobFinishEvent = (args) => {
            if (this.inGarbageTruck)
                this.inGarbageTruck = false;
        };
        this.setBaseBlipEvent = (args) => {
        };
        this.playerExitVehicle = (entity) => {
            if (this.inGarbageTruck)
                this.inGarbageTruck = false;
        };
        this.onUpdate = () => {
            if (this.inGarbageTruck) {
                API.drawText("Current garbage: " + this.currentTruckGarbage + "/" + this.maxTruckGarbage, 300, 770, 0.5, 255, 0, 0, 255, 0, 0, true, false, 0);
            }
        };
        Main.eventHandlers[EVENT_FACTION_GARBAGE_START] = this.jobStartEvent;
        Main.eventHandlers[EVENT_FACTION_GARBAGE_UPDATE] = this.jobUpdateEvent;
        Main.eventHandlers[EVENT_FACTION_GARBAGE_VEHICLE_ENTER] = this.vehicleEnterEvent;
        Main.eventHandlers[EVENT_FACTION_GARBAGE_FINISH] = this.jobFinishEvent;
        Main.eventHandlers[EVENT_FACTION_GARBAGE_SET_BASE_BLIP] = this.setBaseBlipEvent;
        API.onPlayerExitVehicle.connect(this.playerExitVehicle);
        API.onUpdate.connect(this.onUpdate);
    }
}
const EVENT_MDC_POPULATE_SEARCH_RESULTS = "modules_faction_police_mdc_populate_results";
const EVENT_MDC_SEARCH_NOT_FOUND = "modules_faction_police_mdc_search_not_found";
const EVENT_MDC_VIEW = "modules_faction_police_mdc_view";
const EVENT_MDC_WARRANT_ADDED = "modules_faction_police_mdc_warrant_added";
const EVENT_MDC_PLAYER_NO_LONGER_CONNECTED = "modules_faction_police_mdc_player_no_longer_connected";
const EVENT_MDC_CRIMINAL_CHARGE_SHOW = "modules_faction_police_mdc_criminal_charge_show";
const EVENT_MDC_CRIMINAL_RECORD_UPDATE = "modules_faction_police_mdc_criminal_record_update";
class FactionPolice {
    constructor() {
        this.onPopulateSearchResults = (args) => {
            API.sendChatMessage("populating search results");
            var populateType = args[0];
            if (populateType == 0) {
                var playerNameArray = args[1];
                var playerPhoneNumberArray = args[2];
                for (var i = 0; i < playerNameArray.Count; i++) {
                    Main.browser.findBrowserByName("main").eval("addPersonResult('" + playerNameArray[i] + "', '" + playerPhoneNumberArray[i] + "')");
                }
                Main.browser.findBrowserByName("main").eval("showSearchResults(0)");
            }
            else if (populateType == 1) {
                var vehicleNamesArray = args[1];
                var vehiclePlatesArray = args[2];
                var vehicleOwnersArray = args[3];
                for (var i = 0; i < vehicleNamesArray.Count; i++) {
                    Main.browser.findBrowserByName("main").eval("addVehicleResult('" + vehicleNamesArray[i] + "', '" + vehiclePlatesArray[i] + "', '" + vehicleOwnersArray[i] + "')");
                }
                Main.browser.findBrowserByName("main").eval("showSearchResults(1)");
            }
        };
        this.onSearchNotFound = (args) => {
            Main.browser.findBrowserByName("main").eval("showSearchNotFound()");
        };
        this.onView = (args) => {
            var serializedData = args[0];
            Main.browser.findBrowserByName("main").eval("fillPlayerPersonalInfo('" + serializedData + "')");
            Main.browser.findBrowserByName("main").eval("hideAllTabs(); $('#content-personal-info').show(); enableButtons();");
        };
        this.onWarrantAdded = (args) => {
            var reason = args[0];
            Main.browser.findBrowserByName("main").eval("addPlayerWarrant(++lastWarrantNumber, '" + reason + "'); hideAllTabs(); $('#content-warrants').show();");
        };
        this.onPlayerNoLongerConnected = (args) => {
            Main.browser.findBrowserByName("main").eval("hideAllTabs(); $('#content-player-no-longer-connected').show();");
        };
        this.onCriminalChargeShow = (args) => {
            var serializedData = args[0];
            Main.browser.findBrowserByName("main").eval("fillCriminalCharge('" + serializedData + "');");
            Main.browser.findBrowserByName("main").eval("hideAllTabs(); $('#content-criminal-record-view').show();");
        };
        this.onCriminalRecordUpdate = (args) => {
            var chargeDate = args[0];
            var chargeCount = args[1];
            var chargeId = args[2];
            Main.browser.findBrowserByName("main").eval("addCriminalRecord('" + chargeDate + "', " + chargeCount + ", " + chargeId + ");");
        };
        Main.eventHandlers[EVENT_MDC_POPULATE_SEARCH_RESULTS] = this.onPopulateSearchResults;
        Main.eventHandlers[EVENT_MDC_SEARCH_NOT_FOUND] = this.onSearchNotFound;
        Main.eventHandlers[EVENT_MDC_VIEW] = this.onView;
        Main.eventHandlers[EVENT_MDC_WARRANT_ADDED] = this.onWarrantAdded;
        Main.eventHandlers[EVENT_MDC_PLAYER_NO_LONGER_CONNECTED] = this.onPlayerNoLongerConnected;
        Main.eventHandlers[EVENT_MDC_CRIMINAL_CHARGE_SHOW] = this.onCriminalChargeShow;
        Main.eventHandlers[EVENT_MDC_CRIMINAL_RECORD_UPDATE] = this.onCriminalRecordUpdate;
    }
}
const EVENT_BLIP_CREATE = "modules_blip_create";
const EVENT_BLIP_DESTROY = "modules_blip_destroy";
const EVENT_BLIP_SET_COLOR = "modules_blip_setcolor";
const EVENT_BLIP_SET_SPRITE = "modules_blip_setsprite";
class Blip {
    constructor() {
        this.blips = [];
        this.blipCreateEvent = (args) => {
            var blipID = args[0];
            var blipPosition = args[1];
            var blipSprite = args[2];
            var blipColor = args[3];
            if (this.blips[blipID] !== undefined) {
                if (this.blips[blipID] != null) {
                    API.deleteEntity(this.blips[blipID]);
                }
            }
            this.blips[blipID] = API.createBlip(blipPosition);
            API.setBlipSprite(this.blips[blipID], blipSprite);
            API.setBlipColor(this.blips[blipID], blipColor);
        };
        this.blipDestroyEvent = (args) => {
            var blipID = args[0];
            if (this.blips[blipID] === undefined)
                return;
            if (this.blips[blipID] == null)
                return;
            API.deleteEntity(this.blips[blipID]);
        };
        this.blipSetColorEvent = (args) => {
            var blipID = args[0];
            var blipColor = args[1];
            if (this.blips[blipID] === undefined)
                return;
            if (this.blips[blipID] == null)
                return;
            API.setBlipColor(this.blips[blipID], blipColor);
        };
        this.blipSetSpriteEvent = (args) => {
            var blipID = args[0];
            var blipSprite = args[1];
            if (this.blips[blipID] === undefined)
                return;
            if (this.blips[blipID] == null)
                return;
            API.setBlipSprite(this.blips[blipID], blipSprite);
        };
        Main.eventHandlers[EVENT_BLIP_CREATE] = this.blipCreateEvent;
        Main.eventHandlers[EVENT_BLIP_DESTROY] = this.blipDestroyEvent;
        Main.eventHandlers[EVENT_BLIP_SET_COLOR] = this.blipSetColorEvent;
        Main.eventHandlers[EVENT_BLIP_SET_SPRITE] = this.blipSetSpriteEvent;
    }
}
const EVENT_BROWSER_CREATE = "modules_player_browser_create";
const EVENT_BROWSER_DESTROY = "modules_player_browser_destroy";
const EVENT_BROWSER_SETURL = "modules_player_browser_seturl";
const EVENT_BROWSER_SHOW = "modules_player_browser_show";
const EVENT_BROWSER_HIDE = "modules_player_browser_hide";
const EVENT_BROWSER_SET_POSITION = "modules_player_browser_set_position";
const EVENT_BROWSER_SET_SIZE = "modules_player_browser_set_size";
class Browser {
    constructor() {
        this.browsers = [];
        this.browserCreateEvent = (args) => {
            var browserName = args[0];
            var browserResolutionType = args[1];
            var browserWidth = args[2];
            var browserHeight = args[3];
            if (this.findBrowserByName(browserName) != null) {
                this.findBrowserByName(browserName).destroy();
            }
            var createdBrowser = new CefHelper(browserName);
            if (browserResolutionType == 1) {
                var fullResolution = API.getScreenResolution();
                browserWidth = fullResolution.Width;
                browserHeight = fullResolution.Height;
            }
            createdBrowser.create(browserWidth, browserHeight);
            this.browsers.push(createdBrowser);
        };
        this.browserDestroyEvent = (args) => {
            var browserName = args[0];
            var browserIndex = this.findBrowserIndexByName(browserName);
            if (browserIndex == -1) {
                API.sendChatMessage("Attempted to destroy a browser that does not exist. Name: " + browserName);
                return;
            }
            this.browsers[browserIndex].destroy();
            this.browsers.splice(browserIndex, 1);
        };
        this.browserSetUrlEvent = (args) => {
            var browserName = args[0];
            var browserPath = args[1];
            var enableCursor = args[2];
            var show = args[3];
            var browser = this.findBrowserByName(browserName);
            if (browser == null) {
                API.sendChatMessage("SETURL: Browser with the name " + browserName + " was not found.");
                return;
            }
            if (!browser.created) {
                API.sendChatMessage("SETURL: Browser with the name " + browserName + " is not created.");
                return;
            }
            browser.goto(browserPath);
            if (enableCursor)
                API.showCursor(true);
            if (show)
                browser.show();
        };
        this.browserShowEvent = (args) => {
            var browserName = args[0];
            var enableCursor = args[1];
            var browser = this.findBrowserByName(browserName);
            if (browser == null) {
                API.sendChatMessage("SHOW: Browser with the name " + browserName + " was not found.");
                return;
            }
            if (!browser.created) {
                API.sendChatMessage("SHOW: Browser with the name " + browserName + " is not created.");
                return;
            }
            browser.show();
            if (enableCursor)
                API.showCursor(true);
        };
        this.browserHideEvent = (args) => {
            var browserName = args[0];
            var hideCursor = args[1];
            var browser = this.findBrowserByName(browserName);
            if (browser == null) {
                API.sendChatMessage("HIDE: Browser with the name " + browserName + " was not found.");
                return;
            }
            if (!browser.created) {
                API.sendChatMessage("HIDE: Browser with the name " + browserName + " is not created.");
                return;
            }
            browser.hide();
            if (hideCursor)
                API.showCursor(false);
        };
        this.browserSetPositionEvent = (args) => {
            var browserName = args[0];
            var positionX = args[1];
            var positionY = args[2];
            var browser = this.findBrowserByName(browserName);
            if (browser == null) {
                API.sendChatMessage("SETPOSITION: Browser with the name " + browserName + " was not found.");
                return;
            }
            if (!browser.created) {
                API.sendChatMessage("SETPOSITION: Browser with the name " + browserName + " is not created.");
                return;
            }
            browser.setPosition(positionX, positionY);
        };
        this.browserSetSizeEvent = (args) => {
            var browserName = args[0];
            var width = args[1];
            var height = args[2];
            var browser = this.findBrowserByName(browserName);
            if (browser == null) {
                API.sendChatMessage("SETSIZE: Browser with the name " + browserName + " was not found.");
                return;
            }
            if (!browser.created) {
                API.sendChatMessage("SETSIZE: Browser with the name " + browserName + " is not created.");
                return;
            }
            browser.setSize(width, height);
        };
        Main.eventHandlers[EVENT_BROWSER_CREATE] = this.browserCreateEvent;
        Main.eventHandlers[EVENT_BROWSER_DESTROY] = this.browserDestroyEvent;
        Main.eventHandlers[EVENT_BROWSER_SETURL] = this.browserSetUrlEvent;
        Main.eventHandlers[EVENT_BROWSER_SHOW] = this.browserShowEvent;
        Main.eventHandlers[EVENT_BROWSER_HIDE] = this.browserHideEvent;
        Main.eventHandlers[EVENT_BROWSER_SET_POSITION] = this.browserSetPositionEvent;
        Main.eventHandlers[EVENT_BROWSER_SET_SIZE] = this.browserSetSizeEvent;
    }
    findBrowserByName(name) {
        var returnValue = null;
        this.browsers.forEach((val, index, array) => {
            if (val.Name == name)
                returnValue = val;
        });
        return returnValue;
    }
    findBrowserIndexByName(name) {
        var browser = this.findBrowserByName(name);
        if (browser == null)
            return -1;
        return this.browsers.indexOf(browser);
    }
}
const EVENT_MARKER_CREATE = "modules_market_create";
const EVENT_MARKER_DESTROY = "modules_marker_destroy";
const EVENT_MARKER_SET_COLOR = "modules_marker_setcolor";
class Marker {
    constructor() {
        this.markers = [];
        this.markerCreateEvent = (args) => {
            var markerID = args[0];
            var markerType = args[1];
            var markerPosition = args[2];
            var markerColorR = args[3];
            var markerColorG = args[4];
            var markerColorB = args[5];
            var markerColorA = args[6];
            if (this.markers[markerID] !== undefined) {
                if (this.markers[markerID] != null) {
                    API.deleteEntity(this.markers[markerID]);
                }
            }
            this.markers[markerID] = API.createMarker(markerType, markerPosition, new Vector3(), new Vector3(), new Vector3(1, 1, 1), markerColorR, markerColorG, markerColorB, markerColorA);
        };
        this.markerDestroyEvent = (args) => {
            var markerID = args[0];
            if (this.markers[markerID] === undefined)
                return;
            if (this.markers[markerID] == null)
                return;
            API.deleteEntity(this.markers[markerID]);
        };
        this.markerSetColorEvent = (args) => {
            var markerID = args[0];
            if (this.markers[markerID] === undefined)
                return;
            if (this.markers[markerID] == null)
                return;
            var cA = args[1];
            var cR = args[2];
            var cG = args[3];
            var cB = args[4];
            API.setMarkerColor(this.markers[markerID], cA, cR, cG, cB);
        };
        Main.eventHandlers[EVENT_MARKER_CREATE] = this.markerCreateEvent;
        Main.eventHandlers[EVENT_MARKER_DESTROY] = this.markerDestroyEvent;
        Main.eventHandlers[EVENT_MARKER_SET_COLOR] = this.markerSetColorEvent;
    }
}
const EVENT_MENU_CREATE = "modules_menu_show";
const EVENT_MENU_ONITEMSELECT = "modules_menu_onitemselect";
const EVENT_MENU_ONCLOSE = "modules_menu_onclose";
const EVENT_MENU_CLOSE = "modules_menu_close";
class Menu {
    constructor() {
        this.menuCreateEvent = (args) => {
            if (this.currentMenu != null) {
                this.currentMenu.Visible = false;
                this.currentMenu = null;
            }
            var banner = args[0];
            var title = args[1];
            var offsetX = args[2];
            var offsetY = args[3];
            var anchor = args[4];
            var canExit = args[5];
            var itemLabels = args[6];
            var itemDescriptions = args[7];
            this.currentMenu = API.createMenu(banner, title, offsetX, offsetY, anchor);
            for (var i = 0; i < itemLabels.Count; i++) {
                this.currentMenu.AddItem(API.createMenuItem(itemLabels[i], itemDescriptions[i]));
            }
            this.currentMenu.RefreshIndex();
            this.currentMenu.OnItemSelect.connect(this.onItemSelect);
            this.currentMenu.OnMenuClose.connect(this.onMenuClose);
            this.currentMenu.Visible = true;
        };
        this.menuCloseEvent = (args) => {
            if (this.currentMenu != null) {
                this.currentMenu.Visible = false;
                this.currentMenu = null;
            }
        };
        this.onUpdate = () => {
            if (this.currentMenu != null) {
                API.drawMenu(this.currentMenu);
            }
        };
        this.onItemSelect = (sender, item, index) => {
            API.triggerServerEvent(EVENT_MENU_ONITEMSELECT, index);
            this.currentMenu.Visible = false;
            this.currentMenu = null;
        };
        this.onMenuClose = (sender) => {
            API.triggerServerEvent(EVENT_MENU_ONCLOSE);
        };
        this.currentMenu = null;
        Main.eventHandlers[EVENT_MENU_CREATE] = this.menuCreateEvent;
        Main.eventHandlers[EVENT_MENU_CLOSE] = this.menuCloseEvent;
        API.onUpdate.connect(this.onUpdate);
    }
}
const EVENT_PLAYER_MONEY_UPDATE = "modules_player_money_update";
const EVENT_PLAYER_DISABLE_CHAT = "modules_player_disable_chat";
const EVENT_PLAYER_ENABLE_CHAT = "modules_player_enable_chat";
const EVENT_PLAYER_AUTH_START_CAMERA = "modules_player_start_auth_camera";
const EVENT_PLAYER_AUTH_RESET_CAMERA = "modules_player_reset_auth_camera";
const EVENT_PLAYER_ONKEYDOWN = "modules_player_event_onkeydown";
const EVENT_PLAYER_READY_HEARTBEAT = "modules_players_event_ready_heartbeat";
const EVENT_PLAYER_CUSTOMIZATION_CAMERA = "modules_players_event_customization_camera";
const EVENT_PLAYER_CUSTOMIZATION_CAMERA_END = "modules_players_event_customization_camera_end";
const EVENT_PLAYER_GROUND_HEIGHT = "modules_players_event_ground_height";
const EVENT_PLAYER_TOGGLE_AIRBRK = "modules_players_event_toggle_airbrk";
const EVENT_PLAYER_ATTEMPT_ENTRANCE = "modules_players_event_attempt_entrance";
class Player {
    constructor() {
        this.currentMoney = 0;
        this.selectingObject = false;
        this.sentReadyHeartbeat = false;
        this.lastTime = 0;
        this.airbrk = false;
        this.onKeyDown = (sender, e) => {
        };
        this.onKeyUp = (sender, e) => {
            if (this.airbrk) {
                if (e.KeyCode == Keys.W) {
                    var forwardPosition = VMath.getLocalPlayerForwardPosition(0.5);
                    API.setEntityPosition(API.getLocalPlayer(), forwardPosition);
                }
            }
            if (!API.isChatOpen()) {
                if (!API.isPlayerInAnyVehicle(API.getLocalPlayer())) {
                    if (e.KeyCode == Keys.Enter) {
                        API.triggerServerEvent(EVENT_PLAYER_ATTEMPT_ENTRANCE);
                    }
                }
            }
            if (e.KeyCode == Keys.F2) {
                API.showCursor(!API.isCursorShown());
            }
        };
        this.onUpdate = () => {
            API.drawText("$" + this.currentMoney, 1820, 100, 0.6, 0, 255, 0, 255, 0, 2, true, true, 0);
            if (!this.sentReadyHeartbeat) {
                API.triggerServerEvent(EVENT_PLAYER_READY_HEARTBEAT);
                this.sentReadyHeartbeat = true;
            }
            if (this.selectingObject) {
                var cursorPos = API.getCursorPositionMantainRatio();
                var screenToWorld = API.screenToWorldMantainRatio(cursorPos);
                var rayCast = API.createRaycast(API.getGameplayCamPos(), screenToWorld, -1, null);
                if (rayCast.didHitEntity) {
                    var hitEntity = rayCast.hitEntity;
                    API.setEntityTransparency(hitEntity, 100);
                    API.displaySubtitle("Hit entity " + hitEntity.Value);
                }
                if (API.isDisabledControlPressed(24)) {
                    API.showCursor(false);
                    this.selectingObject = false;
                }
            }
        };
        this.moneyUpdateEvent = (args) => {
            this.currentMoney = args[0];
        };
        this.disableChatEvent = (args) => {
            API.setCanOpenChat(false);
        };
        this.enableChatEvent = (args) => {
            API.setCanOpenChat(true);
        };
        this.authStartCameraEvent = (args) => {
            API.setHudVisible(false);
            this.authFromCamera = API.createCamera(new Vector3(431.8062, 845.4288, 233.92), new Vector3(-3.549473, -1.086838, 159.0152));
            this.authToCamera = API.createCamera(new Vector3(-134.3234, -746.7285, 89.73032), new Vector3(0.4361472, -3.680317, 153.9228));
            API.setActiveCamera(this.authFromCamera);
            API.interpolateCameras(this.authFromCamera, this.authToCamera, 120 * 1000, true, true);
        };
        this.authResetCameraEvent = (args) => {
            API.setHudVisible(true);
            API.setGameplayCameraActive();
            API.detachCamera(this.authFromCamera);
            API.detachCamera(this.authToCamera);
        };
        this.customizationCameraEvent = (args) => {
            var position = args[0];
            var pointAt = args[1];
            var fixCamera = API.createCamera(position.Subtract(new Vector3(5, 0, 0)), new Vector3());
            API.pointCameraAtEntity(fixCamera, API.getLocalPlayer(), new Vector3());
            API.setActiveCamera(fixCamera);
            this.customizationCamera = API.createCamera(position, new Vector3());
            API.pointCameraAtPosition(this.customizationCamera, pointAt);
        };
        this.customizationCameraEndEvent = (args) => {
            API.detachCamera(this.customizationCamera);
            API.setGameplayCameraActive();
        };
        this.onGroundHeight = (args) => {
        };
        this.onToggleAirbrk = (args) => {
            var toggleVal = args[0];
            this.airbrk = toggleVal;
            API.setEntityCollissionless(API.getLocalPlayer(), toggleVal);
        };
        Main.eventHandlers[EVENT_PLAYER_MONEY_UPDATE] = this.moneyUpdateEvent;
        Main.eventHandlers[EVENT_PLAYER_DISABLE_CHAT] = this.disableChatEvent;
        Main.eventHandlers[EVENT_PLAYER_ENABLE_CHAT] = this.enableChatEvent;
        Main.eventHandlers[EVENT_PLAYER_AUTH_START_CAMERA] = this.authStartCameraEvent;
        Main.eventHandlers[EVENT_PLAYER_AUTH_RESET_CAMERA] = this.authResetCameraEvent;
        Main.eventHandlers[EVENT_PLAYER_CUSTOMIZATION_CAMERA] = this.customizationCameraEvent;
        Main.eventHandlers[EVENT_PLAYER_CUSTOMIZATION_CAMERA_END] = this.customizationCameraEndEvent;
        Main.eventHandlers[EVENT_PLAYER_GROUND_HEIGHT] = this.onGroundHeight;
        Main.eventHandlers[EVENT_PLAYER_TOGGLE_AIRBRK] = this.onToggleAirbrk;
        Main.eventHandlers["select_object"] = (args) => {
            this.selectingObject = true;
            API.showCursor(true);
        };
        API.onUpdate.connect(this.onUpdate);
        API.onKeyDown.connect(this.onKeyDown);
        API.onKeyUp.connect(this.onKeyUp);
    }
}
const EVENT_SCOREBOARD_PLAYER_ADD = "modules_player_scoreboard_player_add";
const EVENT_SCOREBOARD_PLAYER_REMOVE = "modules_player_scoreboard_player_remove";
const EVENT_SCOREBOARD_PLAYER_UPDATE = "modules_player_scoreboard_player_update";
class Scoreboard {
    constructor() {
        this.scoreboardPlayerAddEvent = (args) => {
            var scoreboardPlayerName = args[0];
            var scoreboardPlayerPing = args[1];
            var scoreboardPlayerID = args[2];
            Main.browser.findBrowserByName("scoreboard").eval("addPlayer('" + scoreboardPlayerName + "', " + scoreboardPlayerPing + ", " + scoreboardPlayerID + ");");
        };
        this.scoreboardPlayerRemoveEvent = (args) => {
            var scoreboardPlayerName = args[0];
            Main.browser.findBrowserByName("scoreboard").eval("removePlayer('" + scoreboardPlayerName + "');");
        };
        this.scoreboardPlayerUpdateEvent = (args) => {
            var scoreboardPlayerName = args[0];
            var scoreboardPlayerPing = args[1];
            Main.browser.findBrowserByName("scoreboard").eval("setPing('" + scoreboardPlayerName + "', " + scoreboardPlayerPing + ");");
        };
        Main.eventHandlers[EVENT_SCOREBOARD_PLAYER_ADD] = this.scoreboardPlayerAddEvent;
        Main.eventHandlers[EVENT_SCOREBOARD_PLAYER_REMOVE] = this.scoreboardPlayerRemoveEvent;
        Main.eventHandlers[EVENT_SCOREBOARD_PLAYER_UPDATE] = this.scoreboardPlayerUpdateEvent;
    }
}
const EVENT_SHOW_PERSONALVEHICLE_BLIP = "modules_vehicle_personalvehicle_blip_show";
const EVENT_TRIGGER_VEHICLE_INDICATORS = "modules_vehicle_trigger_indicator";
class Vehicle {
    constructor() {
        this.showPersonalVehicleBlipEvent = (args) => {
            if (this.personalVehicleBlip != null)
                API.deleteEntity(this.personalVehicleBlip);
            this.personalVehicleBlip = API.createBlip(args[0]);
            API.setBlipSprite(this.personalVehicleBlip, 255);
            API.setBlipColor(this.personalVehicleBlip, 1);
        };
        this.onKeyDown = (sender, e) => {
            if (!API.isChatOpen()) {
                if (e.KeyValue == 188 || e.KeyValue == 190) {
                    if (API.isPlayerInAnyVehicle(API.getLocalPlayer())) {
                        API.triggerServerEvent(EVENT_TRIGGER_VEHICLE_INDICATORS, e.KeyValue);
                    }
                }
            }
        };
        Main.eventHandlers[EVENT_SHOW_PERSONALVEHICLE_BLIP] = this.showPersonalVehicleBlipEvent;
        API.onKeyDown.connect(this.onKeyDown);
    }
}
const EVENT_DEALERSHIP_SHOW_MENU = "modules_vehicle_dealership_menu_show";
const EVENT_DEALERSHIP_SHOW_BUY_MENU = "modules_vehicle_dealership_buy_menu_show";
const EVENT_DEALERSHIP_BUY_SELECT = "modules_vehicle_dealership_buy_select";
const EVENT_DEALERSHIP_BUY_MENU_PURCHASE = "modules_vehicle_dealership_buy_menu_purchase";
class VehicleDealership {
    constructor() {
        this.dealershipMenu = null;
        this.lastDealershipMenuId = 0;
        this.dealershipBuyMenu = null;
        this.lastDealershipBuyMenuId = 0;
        this.lastDealershipBuyMenuVehicleIndex = 0;
        this.onUpdate = () => {
            if (this.dealershipMenu != null)
                API.drawMenu(this.dealershipMenu);
            else if (this.dealershipBuyMenu != null)
                API.drawMenu(this.dealershipBuyMenu);
        };
        this.showDealershipMenuEvent = (args) => {
            this.dealershipMenu = API.createMenu("Dealership", 0, 0, 6);
            this.lastDealershipMenuId = args[3];
            this.dealershipMenu.OnItemSelect.connect(this.onDealershipMenuItemSelect);
            this.dealershipMenu.ResetKey(NativeUI.UIMenu_MenuControls.Back);
            API.setMenuSubtitle(this.dealershipMenu, args[0]);
            this.dealershipMenu.AddItem(API.createMenuItem("Exit", ""));
            for (var i = 0; i < args[1].Count; i++) {
                this.dealershipMenu.AddItem(API.createMenuItem("" + args[1][i] + " - $" + args[2][i], ""));
            }
            this.dealershipMenu.Visible = true;
            API.showCursor(true);
        };
        this.onDealershipMenuItemSelect = (sender, item, index) => {
            API.showCursor(false);
            this.dealershipMenu.Visible = false;
            if (index > 0) {
                API.triggerServerEvent(EVENT_DEALERSHIP_BUY_SELECT, index - 1, this.lastDealershipMenuId);
            }
        };
        this.showDealershipBuyMenuEvent = (args) => {
            this.dealershipBuyMenu = API.createMenu("" + args[1], 0, 0, 6);
            this.lastDealershipBuyMenuId = args[0];
            this.lastDealershipBuyMenuVehicleIndex = args[3];
            this.dealershipBuyMenu.OnItemSelect.connect(this.onDealershipBuyMenuItemSelect);
            this.dealershipBuyMenu.ResetKey(NativeUI.UIMenu_MenuControls.Back);
            this.dealershipBuyMenu.AddItem(API.createMenuItem("Exit", "Exit the dealership menu"));
            this.dealershipBuyMenu.AddItem(API.createMenuItem("Purchase - $" + args[2], "Purchase the vehicle"));
            this.dealershipBuyMenu.Visible = true;
            API.showCursor(true);
        };
        this.onDealershipBuyMenuItemSelect = (sender, item, index) => {
            API.showCursor(false);
            this.dealershipBuyMenu.Visible = false;
            if (index == 1) {
                API.triggerServerEvent(EVENT_DEALERSHIP_BUY_MENU_PURCHASE, this.lastDealershipBuyMenuId, this.lastDealershipBuyMenuVehicleIndex);
            }
        };
        Main.eventHandlers[EVENT_DEALERSHIP_SHOW_MENU] = this.showDealershipMenuEvent;
        Main.eventHandlers[EVENT_DEALERSHIP_SHOW_BUY_MENU] = this.showDealershipBuyMenuEvent;
        API.onUpdate.connect(this.onUpdate);
    }
}
